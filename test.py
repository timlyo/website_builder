from build import *


def test_remove():
    string = "thing/to_remove"
    result = remove_from_string(string, "to_remove")

    assert result == "thing/"

    result = remove_from_string(string, ["/", "to_remove"])

    assert result == "thing"


def test_get_articles():
    articles = get_articles()

    assert articles[0] == {
        "url": "example.html",
        "title": "example",
        "date": "1066/02/22",
        "outline": "outline",
    }


