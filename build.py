from jinja2 import Environment, Template, PackageLoader, select_autoescape, FileSystemLoader
import os
from glob import glob
from markdown2 import Markdown
from subprocess import call

# Config
# Base page that all article pages are mushed into
# Must include a {{contents | safe }} block html is passed into this
article_template_name = "article.html"
# Base page that the list of articles are mushed into
# passed a list called articles
article_list_page_name = "article_list.html"
output_dir = "output"
article_output_dir = "{}/articles".format(output_dir)
css_output_dir = "{}/css".format(output_dir)
files_output_dir = "{}/files".format(output_dir)

# Directory containing normal html files
template_directory = "pages"
# Directory containing renderable md files
articles_directory = "articles"

env = Environment(
    loader=FileSystemLoader("pages"),
    autoescape=select_autoescape(['html'])
)


def remove_from_string(string: str, patterns):
    if not isinstance(patterns, list):
        patterns = [patterns]

    for pattern in patterns:
        string = string.replace(pattern, "")

    return string


def render_template(name, **kwargs):
    print("Rendering", name)
    with open("{}/{}".format(output_dir, name), "w") as file:
        file.write(env.get_template(name).render(kwargs))


def render_other_html():
    html = filter(
        lambda x: x not in [article_template_name, article_list_page_name],
        [remove_from_string(x, [template_directory, "/"]) for x in glob("pages/*.html")]
    )

    for page in html:
        render_template(page)


def file_to_dict(name) -> dict:
    markdown = Markdown(extras=["tables", "metadata", "footnotes", "toc"])
    metadata = markdown.convert(open(name).read()).metadata
    if not metadata.get("public"):
        return
    name = remove_from_string(name, [articles_directory, "/"])
    return {
        "url": name.replace(".md", ".html"),
        "title": name.replace("_", " ").replace(".md", ""),
        "date": metadata.get("date"),
        "outline": metadata.get("outline"),
    }


def get_articles() -> list:
    all_articles = glob("articles/*")

    valid_articles = list(
        filter(
            lambda a: a is not None,
            [file_to_dict(a) for a in all_articles]
        ))

    return sorted(valid_articles, key=lambda x: x["date"], reverse=True)


def render_articles():
    for article in glob("articles/*"):
        markdown = Markdown(extras=["tables", "metadata", "footnotes", "toc"])
        print("Rendering", article)
        name = article.replace("articles/", "").replace(".md", ".html")
        with open(article) as input:
            contents = markdown.convert(input.read())
            toc = contents.toc_html
            with open("output/articles/" + name, "w") as output:
                html = env.get_template("article.html").render(contents=contents, toc=toc)
                output.write(html)


def compile_sass():
    for style in glob("sass/*"):
        print("Compiling", style)
        output_file = "output/css/" + style.replace("sass", "") + "css"
        call(["sassc", style, output_file])


def copy_files():
    print("Copying files")
    call(["cp", "-r", "files", "output"])


if __name__ == "__main__":
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    if not os.path.exists(article_output_dir):
        os.makedirs(article_output_dir)
    if not os.path.exists(css_output_dir):
        os.makedirs(css_output_dir)

    render_articles()
    compile_sass()

    render_other_html()

    articles = get_articles()

    print("Rendering article list with {} public articles".format(len(articles)))
    render_template("article_list.html", articles=articles)

    copy_files()
